\select@language {czech}
\contentsline {part}{I\hspace {1em}Sezn\IeC {\'a}men\IeC {\'\i } s Arduinem}{}{part.1}
\contentsline {chapter}{\numberline {1}O Arduinu}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Typy desek}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Arduino Mini}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Arduino Nano}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Arduino Micro}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}LilyPad Arduino}{5}{section.2.4}
\contentsline {section}{\numberline {2.5}Arduino Fio}{5}{section.2.5}
\contentsline {section}{\numberline {2.6}Arduino Uno}{6}{section.2.6}
\contentsline {section}{\numberline {2.7}Arduino Leonardo}{6}{section.2.7}
\contentsline {section}{\numberline {2.8}Arduino Y\IeC {\'u}n}{7}{section.2.8}
\contentsline {section}{\numberline {2.9}Arduino Mega2560}{7}{section.2.9}
\contentsline {section}{\numberline {2.10}Arduino Due}{8}{section.2.10}
\contentsline {section}{\numberline {2.11}Arduino Esplora}{8}{section.2.11}
\contentsline {section}{\numberline {2.12}Arduino Robot}{9}{section.2.12}
\contentsline {section}{\numberline {2.13}Arduino Intel Galileo}{9}{section.2.13}
\contentsline {section}{\numberline {2.14}Arduino Tre}{10}{section.2.14}
\contentsline {chapter}{\numberline {3}Arduino Shieldy}{11}{chapter.3}
\contentsline {chapter}{\numberline {4}Arduino klony}{12}{chapter.4}
\contentsline {part}{II\hspace {1em}Programujeme Arduino}{13}{part.2}
\contentsline {chapter}{\numberline {5}V\IeC {\'y}b\IeC {\v e}r a sezn\IeC {\'a}men\IeC {\'\i }}{15}{chapter.5}
\contentsline {chapter}{\numberline {6}Arduino IDE}{17}{chapter.6}
\contentsline {section}{\numberline {6.1}Historie}{17}{section.6.1}
\contentsline {section}{\numberline {6.2}Sta\IeC {\v z}en\IeC {\'\i } a instalace}{17}{section.6.2}
\contentsline {section}{\numberline {6.3}Pou\IeC {\v z}\IeC {\'\i }v\IeC {\'a}n\IeC {\'\i }}{18}{section.6.3}
\contentsline {section}{\numberline {6.4}Programovac\IeC {\'\i } jazyk}{19}{section.6.4}
\contentsline {chapter}{\numberline {7}Blik\IeC {\'a}me LED}{20}{chapter.7}
\contentsline {section}{\numberline {7.1}Budeme pot\IeC {\v r}ebovat}{20}{section.7.1}
\contentsline {section}{\numberline {7.2}P\IeC {\v r}ipojen\IeC {\'\i } Arduina a PC}{20}{section.7.2}
\contentsline {section}{\numberline {7.3}Zapojen\IeC {\'\i }}{21}{section.7.3}
\contentsline {section}{\numberline {7.4}Program}{21}{section.7.4}
\contentsline {part}{III\hspace {1em}Z\IeC {\'a}kladn\IeC {\'\i } struktury jazyka Wiring}{22}{part.3}
\contentsline {chapter}{\numberline {8}S\IeC {\'e}riov\IeC {\'a} komunikace}{24}{chapter.8}
\contentsline {chapter}{\numberline {9}Prom\IeC {\v e}nn\IeC {\'e}}{26}{chapter.9}
\contentsline {section}{\numberline {9.1}Pr\IeC {\'a}ce s prom\IeC {\v e}nn\IeC {\'y}mi}{27}{section.9.1}
\contentsline {section}{\numberline {9.2}Datov\IeC {\'e} typy}{28}{section.9.2}
\contentsline {chapter}{\numberline {10}Pole}{29}{chapter.10}
\contentsline {section}{\numberline {10.1}Deklarace pole}{29}{section.10.1}
\contentsline {section}{\numberline {10.2}P\IeC {\v r}\IeC {\'\i }stup k hodnot\IeC {\'a}m v poli}{29}{section.10.2}
\contentsline {chapter}{\numberline {11}Digit\IeC {\'a}ln\IeC {\'\i } vstup a v\IeC {\'y}stup}{30}{chapter.11}
\contentsline {section}{\numberline {11.1}Vstup nebo v\IeC {\'y}stup?}{30}{section.11.1}
\contentsline {section}{\numberline {11.2}Ovl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } v\IeC {\'y}stupu}{30}{section.11.2}
\contentsline {section}{\numberline {11.3}\IeC {\v C}ten\IeC {\'\i } vstupu}{31}{section.11.3}
\contentsline {chapter}{\numberline {12}P\IeC {\v r}\IeC {\'\i }klad: Tla\IeC {\v c}\IeC {\'\i }tko a LED dioda}{32}{chapter.12}
\contentsline {part}{IV\hspace {1em}Pokro\IeC {\v c}ilej\IeC {\v s}\IeC {\'\i } struktury jazyka Wiring}{34}{part.4}
\contentsline {chapter}{\numberline {13}Konstanty}{36}{chapter.13}
\contentsline {section}{\numberline {13.1}Logick\IeC {\'e} konstanty}{36}{section.13.1}
\contentsline {section}{\numberline {13.2}Typ digit\IeC {\'a}ln\IeC {\'\i }ho pinu}{36}{section.13.2}
\contentsline {section}{\numberline {13.3}Proud na digit\IeC {\'a}ln\IeC {\'\i }ch pinech}{38}{section.13.3}
\contentsline {chapter}{\numberline {14}Analogov\IeC {\'y} vstup a v\IeC {\'y}stup}{39}{chapter.14}
\contentsline {section}{\numberline {14.1}analogWrite()}{39}{section.14.1}
\contentsline {section}{\numberline {14.2}analogRead()}{40}{section.14.2}
\contentsline {chapter}{\numberline {15}P\IeC {\v r}\IeC {\'\i }klad: Regulace jasu LED}{41}{chapter.15}
\contentsline {chapter}{\numberline {16}Podm\IeC {\'\i }nky}{43}{chapter.16}
\contentsline {section}{\numberline {16.1}Porovn\IeC {\'a}vac\IeC {\'\i } oper\IeC {\'a}tory}{43}{section.16.1}
\contentsline {section}{\numberline {16.2}Slo\IeC {\v z}en\IeC {\'e} podm\IeC {\'\i }nky}{44}{section.16.2}
\contentsline {section}{\numberline {16.3}if()}{45}{section.16.3}
\contentsline {section}{\numberline {16.4}else if()}{45}{section.16.4}
\contentsline {section}{\numberline {16.5}else}{45}{section.16.5}
\contentsline {section}{\numberline {16.6}Switch}{46}{section.16.6}
\contentsline {chapter}{\numberline {17}P\IeC {\v r}\IeC {\'\i }klad: P\IeC {\'a}s LED diod}{47}{chapter.17}
\contentsline {part}{V\hspace {1em}S\IeC {\'e}riov\IeC {\'a} komunikace a cykly}{49}{part.5}
\contentsline {chapter}{\numberline {18}S\IeC {\'e}riov\IeC {\'a} komunikace}{51}{chapter.18}
\contentsline {section}{\numberline {18.1}Zah\IeC {\'a}jen\IeC {\'\i } komunikace \IeC {\textendash } Serial.begin()}{51}{section.18.1}
\contentsline {section}{\numberline {18.2}Odesl\IeC {\'a}n\IeC {\'\i } dat \IeC {\textendash } Serial.print() a Serial.println()}{52}{section.18.2}
\contentsline {section}{\numberline {18.3}\IeC {\v C}ten\IeC {\'\i } dat \IeC {\textendash } Serial.available() a Serial.read()}{53}{section.18.3}
\contentsline {section}{\numberline {18.4}Ukon\IeC {\v c}en\IeC {\'\i } komunikace \IeC {\textendash } Serial.end()}{54}{section.18.4}
\contentsline {chapter}{\numberline {19}Cykly}{55}{chapter.19}
\contentsline {section}{\numberline {19.1}Slo\IeC {\v z}en\IeC {\'e} oper\IeC {\'a}tory}{55}{section.19.1}
\contentsline {section}{\numberline {19.2}Cyklus while()}{56}{section.19.2}
\contentsline {section}{\numberline {19.3}Cyklus do\IeC {\textellipsis }while()}{57}{section.19.3}
\contentsline {section}{\numberline {19.4}Cyklus for()}{58}{section.19.4}
\contentsline {chapter}{\numberline {20}P\IeC {\v r}\IeC {\'\i }klad: Had z LED diod}{59}{chapter.20}
\contentsline {part}{VI\hspace {1em}U\IeC {\v z}ite\IeC {\v c}n\IeC {\'e} funkce}{60}{part.6}
\contentsline {chapter}{\numberline {21}\IeC {\v C}as}{62}{chapter.21}
\contentsline {section}{\numberline {21.1}delay()}{62}{section.21.1}
\contentsline {section}{\numberline {21.2}delayMicroseconds()}{63}{section.21.2}
\contentsline {section}{\numberline {21.3}millis()}{63}{section.21.3}
\contentsline {section}{\numberline {21.4}micros()}{63}{section.21.4}
\contentsline {chapter}{\numberline {22}Matematick\IeC {\'e} funkce}{64}{chapter.22}
\contentsline {section}{\numberline {22.1}Matematick\IeC {\'e} oper\IeC {\'a}tory}{64}{section.22.1}
\contentsline {section}{\numberline {22.2}min()}{65}{section.22.2}
\contentsline {section}{\numberline {22.3}max()}{65}{section.22.3}
\contentsline {section}{\numberline {22.4}abs()}{66}{section.22.4}
\contentsline {section}{\numberline {22.5}constrain()}{66}{section.22.5}
\contentsline {section}{\numberline {22.6}map()}{66}{section.22.6}
\contentsline {section}{\numberline {22.7}pow()}{67}{section.22.7}
\contentsline {section}{\numberline {22.8}sqrt()}{67}{section.22.8}
\contentsline {section}{\numberline {22.9}Goniometrick\IeC {\'e} funkce}{67}{section.22.9}
\contentsline {subsection}{\numberline {22.9.1}sin()}{68}{subsection.22.9.1}
\contentsline {subsection}{\numberline {22.9.2}cos()}{69}{subsection.22.9.2}
\contentsline {subsection}{\numberline {22.9.3}tan()}{70}{subsection.22.9.3}
\contentsline {chapter}{\numberline {23}N\IeC {\'a}hodn\IeC {\'a} \IeC {\v c}\IeC {\'\i }sla}{71}{chapter.23}
\contentsline {section}{\numberline {23.1}random() a randomSeed()}{71}{section.23.1}
\contentsline {chapter}{\numberline {24}P\IeC {\v r}\IeC {\'\i }klad: Hrac\IeC {\'\i } kostka}{72}{chapter.24}
\contentsline {part}{VII\hspace {1em}U\IeC {\v z}ivatelsky definovan\IeC {\'e} funkce}{75}{part.7}
\contentsline {chapter}{\numberline {25}Definice funkce}{77}{chapter.25}
\contentsline {chapter}{\numberline {26}Vol\IeC {\'a}n\IeC {\'\i } funkce}{78}{chapter.26}
\contentsline {chapter}{\numberline {27}Funkce, kter\IeC {\'e} vrac\IeC {\'\i } hodnotu}{79}{chapter.27}
\contentsline {chapter}{\numberline {28}P\IeC {\v r}evody datov\IeC {\'y}ch typ\IeC {\r u}}{81}{chapter.28}
\contentsline {section}{\numberline {28.1}char()}{81}{section.28.1}
\contentsline {section}{\numberline {28.2}byte()}{81}{section.28.2}
\contentsline {section}{\numberline {28.3}int(), long(), float()}{81}{section.28.3}
\contentsline {chapter}{\numberline {29}Zvuk a t\IeC {\'o}n}{82}{chapter.29}
\contentsline {section}{\numberline {29.1}tone()}{83}{section.29.1}
\contentsline {section}{\numberline {29.2}noTone()}{83}{section.29.2}
\contentsline {section}{\numberline {29.3}P\IeC {\v r}\IeC {\'\i }klad: T\IeC {\v r}\IeC {\'\i }t\IeC {\'o}nov\IeC {\'y} bzu\IeC {\v c}\IeC {\'a}k}{84}{section.29.3}
\contentsline {chapter}{\numberline {30}Segmentov\IeC {\'e} displeje}{85}{chapter.30}
\contentsline {section}{\numberline {30.1}Sedmisegmentov\IeC {\'y} displej}{85}{section.30.1}
\contentsline {section}{\numberline {30.2}V\IeC {\'\i }cesegmentov\IeC {\'e} displeje}{88}{section.30.2}
\contentsline {chapter}{\numberline {31}P\IeC {\v r}\IeC {\'\i }klad: Klav\IeC {\'\i }r}{89}{chapter.31}
\contentsline {part}{VIII\hspace {1em}Arduino jako kl\IeC {\'a}vesnice a my\IeC {\v s}}{91}{part.8}
\contentsline {chapter}{\numberline {32}\IeC {\'U}vod}{93}{chapter.32}
\contentsline {chapter}{\numberline {33}Arduino Leonardo}{94}{chapter.33}
\contentsline {chapter}{\numberline {34}Mouse}{95}{chapter.34}
\contentsline {section}{\numberline {34.1}Mouse.click()}{96}{section.34.1}
\contentsline {section}{\numberline {34.2}Mouse.move()}{97}{section.34.2}
\contentsline {section}{\numberline {34.3}Mouse.press(), Mouse.release() a Mouse.isPressed()}{97}{section.34.3}
\contentsline {section}{\numberline {34.4}P\IeC {\v r}\IeC {\'\i }klad: My\IeC {\v s}}{97}{section.34.4}
\contentsline {chapter}{\numberline {35}Keyboard}{99}{chapter.35}
\contentsline {section}{\numberline {35.1}Keyboard.write()}{99}{section.35.1}
\contentsline {section}{\numberline {35.2}Keyboard.press(), Keyboard.release() a Keyboard.releaseAll()}{100}{section.35.2}
\contentsline {section}{\numberline {35.3}Keyboard.print() a Keyboard.println()}{101}{section.35.3}
\contentsline {chapter}{\numberline {36}Arduino Esplora}{102}{chapter.36}
\contentsline {section}{\numberline {36.1}Joystick}{103}{section.36.1}
\contentsline {section}{\numberline {36.2}Sm\IeC {\v e}rov\IeC {\'a} tla\IeC {\v c}\IeC {\'\i }tka}{103}{section.36.2}
\contentsline {section}{\numberline {36.3}Line\IeC {\'a}rn\IeC {\'\i } potenciometr}{104}{section.36.3}
\contentsline {section}{\numberline {36.4}Mikrofon}{105}{section.36.4}
\contentsline {section}{\numberline {36.5}Sv\IeC {\v e}teln\IeC {\'y} senzor}{105}{section.36.5}
\contentsline {section}{\numberline {36.6}Teplom\IeC {\v e}r}{105}{section.36.6}
\contentsline {section}{\numberline {36.7}Akcelerometr}{106}{section.36.7}
\contentsline {section}{\numberline {36.8}Piezo bzu\IeC {\v c}\IeC {\'a}k}{107}{section.36.8}
\contentsline {section}{\numberline {36.9}RGB LED}{107}{section.36.9}
\contentsline {section}{\numberline {36.10}Neofici\IeC {\'a}ln\IeC {\'\i } pinout}{107}{section.36.10}
\contentsline {part}{IX\hspace {1em}Processing}{108}{part.9}
\contentsline {chapter}{\numberline {37}\IeC {\'U}vod}{110}{chapter.37}
\contentsline {chapter}{\numberline {38}Sezn\IeC {\'a}men\IeC {\'\i }}{111}{chapter.38}
\contentsline {chapter}{\numberline {39}Z\IeC {\'a}klad}{112}{chapter.39}
\contentsline {section}{\numberline {39.1}Datov\IeC {\'e} typy}{112}{section.39.1}
\contentsline {section}{\numberline {39.2}Pole}{112}{section.39.2}
\contentsline {section}{\numberline {39.3}V\IeC {\'y}pis hodnot}{113}{section.39.3}
\contentsline {chapter}{\numberline {40}Kreslen\IeC {\'\i }}{114}{chapter.40}
\contentsline {section}{\numberline {40.1}Vytvo\IeC {\v r}en\IeC {\'\i } kresl\IeC {\'\i }c\IeC {\'\i }ho pl\IeC {\'a}tna}{114}{section.40.1}
\contentsline {section}{\numberline {40.2}Barvy}{115}{section.40.2}
\contentsline {section}{\numberline {40.3}Bod}{116}{section.40.3}
\contentsline {section}{\numberline {40.4}\IeC {\'U}se\IeC {\v c}ka}{117}{section.40.4}
\contentsline {section}{\numberline {40.5}\IeC {\v C}ty\IeC {\v r}\IeC {\r u}heln\IeC {\'\i }k}{118}{section.40.5}
\contentsline {section}{\numberline {40.6}Zvl\IeC {\'a}\IeC {\v s}tn\IeC {\'\i } p\IeC {\v r}\IeC {\'\i }pady \IeC {\v c}ty\IeC {\v r}\IeC {\'u}heln\IeC {\'\i }k\IeC {\r u}}{119}{section.40.6}
\contentsline {section}{\numberline {40.7}Troj\IeC {\'u}heln\IeC {\'\i }k}{122}{section.40.7}
\contentsline {section}{\numberline {40.8}Elipsa a kru\IeC {\v z}nice}{123}{section.40.8}
\contentsline {section}{\numberline {40.9}\IeC {\v C}\IeC {\'a}st kru\IeC {\v z}nice a elipsy}{124}{section.40.9}
\contentsline {chapter}{\numberline {41}Interakce s my\IeC {\v s}\IeC {\'\i }}{125}{chapter.41}
\contentsline {section}{\numberline {41.1}Tla\IeC {\v c}\IeC {\'\i }tka my\IeC {\v s}i}{125}{section.41.1}
\contentsline {section}{\numberline {41.2}Poloha kurzoru}{126}{section.41.2}
\contentsline {part}{X\hspace {1em}Arduino a Processsing}{127}{part.10}
\contentsline {chapter}{\numberline {42}\IeC {\'U}vod}{129}{chapter.42}
\contentsline {chapter}{\numberline {43}Firmata}{130}{chapter.43}
\contentsline {section}{\numberline {43.1}Nastaven\IeC {\'\i }}{130}{section.43.1}
\contentsline {section}{\numberline {43.2}Propojen\IeC {\'\i }}{130}{section.43.2}
\contentsline {section}{\numberline {43.3}Programov\IeC {\'a}n\IeC {\'\i }}{131}{section.43.3}
\contentsline {chapter}{\numberline {44}Vlastn\IeC {\'\i } zp\IeC {\r u}sob komunikace}{134}{chapter.44}
\contentsline {section}{\numberline {44.1}S\IeC {\'e}riov\IeC {\'a} komunikace}{134}{section.44.1}
\contentsline {chapter}{\numberline {45}P\IeC {\v r}\IeC {\'\i }klad: Ovl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } bodu joystickem}{138}{chapter.45}
\contentsline {chapter}{\numberline {46}P\IeC {\v r}\IeC {\'\i }klad: Graf hodnot ze slideru}{140}{chapter.46}
\contentsline {part}{XI\hspace {1em}Propojujeme Arduino s jin\IeC {\'y}mi za\IeC {\v r}\IeC {\'\i }zen\IeC {\'\i }mi}{142}{part.11}
\contentsline {chapter}{\numberline {47}S\IeC {\'e}riov\IeC {\'a} linka}{144}{chapter.47}
\contentsline {section}{\numberline {47.1}Propojen\IeC {\'\i }}{144}{section.47.1}
\contentsline {chapter}{\numberline {48}Bluetooth}{147}{chapter.48}
\contentsline {section}{\numberline {48.1}Odesl\IeC {\'a}n\IeC {\'\i } a zpracov\IeC {\'a}n\IeC {\'\i } dat z potenciometru}{148}{section.48.1}
\contentsline {chapter}{\numberline {49}Arduino a Android}{149}{chapter.49}
\contentsline {chapter}{\numberline {50}Sb\IeC {\v e}rnice i2c}{152}{chapter.50}
\contentsline {section}{\numberline {50.1}Funkce pro pr\IeC {\'a}ci s i2c}{153}{section.50.1}
\contentsline {section}{\numberline {50.2}P\IeC {\v r}enos master $\rightarrow $ slave}{154}{section.50.2}
\contentsline {section}{\numberline {50.3}P\IeC {\v r}enos slave $\rightarrow $ master}{155}{section.50.3}
\contentsline {part}{XII\hspace {1em}Arduino a displeje}{157}{part.12}
\contentsline {chapter}{\numberline {51}\IeC {\'U}vod}{159}{chapter.51}
\contentsline {chapter}{\numberline {52}Maticov\IeC {\'e} LED displeje}{160}{chapter.52}
\contentsline {section}{\numberline {52.1}Teorie \IeC {\v r}\IeC {\'\i }zen\IeC {\'\i }}{161}{section.52.1}
\contentsline {section}{\numberline {52.2}Zapojen\IeC {\'\i }}{162}{section.52.2}
\contentsline {section}{\numberline {52.3}Programov\IeC {\'a}n\IeC {\'\i }}{162}{section.52.3}
\contentsline {chapter}{\numberline {53}RGB teoreticky}{165}{chapter.53}
\contentsline {chapter}{\numberline {54}Rainbowduino}{166}{chapter.54}
\contentsline {section}{\numberline {54.1}Funkce}{167}{section.54.1}
\contentsline {section}{\numberline {54.2}Propojujeme Rainbowduina}{168}{section.54.2}
\contentsline {section}{\numberline {54.3}Zobrazen\IeC {\'\i } obr\IeC {\'a}zku z PC}{171}{section.54.3}
\contentsline {chapter}{\numberline {55}LCD displeje}{177}{chapter.55}
\contentsline {section}{\numberline {55.1}Grafick\IeC {\'e} monochromatick\IeC {\'e} LCD}{183}{section.55.1}
\contentsline {section}{\numberline {55.2}Barevn\IeC {\'e} grafick\IeC {\'e} LCD}{185}{section.55.2}
\contentsline {part}{XIII\hspace {1em}Projekt: 2048}{191}{part.13}
\contentsline {chapter}{\numberline {56}SD karta}{193}{chapter.56}
\contentsline {section}{\numberline {56.1}P\IeC {\v r}\IeC {\'\i }prava Arduina}{194}{section.56.1}
\contentsline {section}{\numberline {56.2}Funkce}{194}{section.56.2}
\contentsline {section}{\numberline {56.3}P\IeC {\v r}\IeC {\'\i }klad 1.: Z\IeC {\'a}pis hodnot}{196}{section.56.3}
\contentsline {section}{\numberline {56.4}P\IeC {\v r}\IeC {\'\i }klad 2.: V\IeC {\'y}pis dat ze souboru}{197}{section.56.4}
\contentsline {chapter}{\numberline {57}Hra 2048}{198}{chapter.57}
\contentsline {section}{\numberline {57.1}Hodnoty}{198}{section.57.1}
\contentsline {section}{\numberline {57.2}Jdeme na to}{199}{section.57.2}
\contentsline {part}{XIV\hspace {1em}Arduino a Ethernet shield}{210}{part.14}
\contentsline {chapter}{\numberline {58}Ethernet Shield}{212}{chapter.58}
\contentsline {section}{\numberline {58.1}Funkce}{214}{section.58.1}
\contentsline {section}{\numberline {58.2}Vytv\IeC {\'a}\IeC {\v r}\IeC {\'\i }me server}{216}{section.58.2}
\contentsline {section}{\numberline {58.3}Sos\IeC {\'a}me data}{219}{section.58.3}
\contentsline {section}{\numberline {58.4}Ovl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } p\IeC {\v r}es s\IeC {\'\i }\IeC {\v t}}{220}{section.58.4}
\contentsline {part}{XV\hspace {1em}N\IeC {\'a}\IeC {\v s} prvn\IeC {\'\i } klon Arduina}{223}{part.15}
\contentsline {chapter}{\numberline {59}P\IeC {\v r}\IeC {\'\i }prava Arduina}{225}{chapter.59}
\contentsline {chapter}{\numberline {60}\IeC {\v C}ipy ATtiny}{226}{chapter.60}
\contentsline {chapter}{\numberline {61}\IeC {\v C}ipy ATmega}{229}{chapter.61}
\contentsline {part}{XVI\hspace {1em}Projekt: Program\IeC {\'a}torsk\IeC {\'a} kl\IeC {\'a}vesnice}{231}{part.16}
\contentsline {chapter}{\numberline {62}Keypad}{233}{chapter.62}
\contentsline {section}{\numberline {62.1}Zapojen\IeC {\'\i } a programov\IeC {\'a}n\IeC {\'\i }}{234}{section.62.1}
\contentsline {chapter}{\numberline {63}Bezpe\IeC {\v c}nostn\IeC {\'\i } syst\IeC {\'e}m}{236}{chapter.63}
\contentsline {chapter}{\numberline {64}Program\IeC {\'a}torsk\IeC {\'a} kl\IeC {\'a}vesnice}{239}{chapter.64}
\contentsline {part}{XVII\hspace {1em}Projekt: Robotick\IeC {\'a} ruka}{241}{part.17}
\contentsline {chapter}{\numberline {65}Servomotory}{242}{chapter.65}
\contentsline {chapter}{\numberline {66}Robotick\IeC {\'a} ruka}{243}{chapter.66}
\contentsline {part}{XVIII\hspace {1em}WiFi shield}{251}{part.18}
\contentsline {chapter}{\numberline {67}Sezn\IeC {\'a}men\IeC {\'\i }}{252}{chapter.67}
\contentsline {chapter}{\numberline {68}Firmware shieldu}{254}{chapter.68}
\contentsline {section}{\numberline {68.1}Zji\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } verze firmware}{254}{section.68.1}
\contentsline {section}{\numberline {68.2}Aktualizace firmware}{255}{section.68.2}
\contentsline {chapter}{\numberline {69}\IeC {\'U}daje pot\IeC {\v r}ebn\IeC {\'e} pro p\IeC {\v r}ipojen\IeC {\'\i } k WiFi}{258}{chapter.69}
\contentsline {chapter}{\numberline {70}P\IeC {\v r}ehled funkc\IeC {\'\i } pro pr\IeC {\'a}ci s WiFi}{259}{chapter.70}
\contentsline {section}{\numberline {70.1}T\IeC {\v r}\IeC {\'\i }da WiFi}{260}{section.70.1}
\contentsline {section}{\numberline {70.2}T\IeC {\v r}\IeC {\'\i }da WiFiServer}{261}{section.70.2}
\contentsline {section}{\numberline {70.3}T\IeC {\v r}\IeC {\'\i }da WiFiClient}{262}{section.70.3}
\contentsline {chapter}{\numberline {71}P\IeC {\v r}\IeC {\'\i }klady}{263}{chapter.71}
\contentsline {section}{\numberline {71.1}P\IeC {\v r}ipojen\IeC {\'\i } k s\IeC {\'\i }ti}{263}{section.71.1}
\contentsline {section}{\numberline {71.2}Interakce se serverem}{265}{section.71.2}
\contentsline {part}{XIX\hspace {1em}Zdroje obr\IeC {\'a}zk\IeC {\r u}}{267}{part.19}
